from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup 
from aiogram import types
from aiogram.dispatcher import Dispatcher
from create_bot import dp, bot
from aiogram.dispatcher.filters import Text
from keyboards import client_keyboard
from data_base import pizza_db

class FSMClient(StatesGroup):
	first_q = State()
	second_q = State()
	last_answer = State()

#машина состояний
#начало диалога
async def  make_changes_command(message : types.Message):
	await bot.send_message(message.from_user.id, 'Приветсвую Вас!', reply_markup=client_keyboard.kb_client)
	await message.delete()
#запоминаем ответ на 1 вопрос
async def start_convers(message : types.Message, state : FSMContext):
	await FSMClient.first_q.set()
	await message.reply('Какую Вы хотите пиццу? Большую или маленькую?')
#запоминаем ответ на 2 вопрос
async def load_first(message : types.Message, state : FSMContext):
	async with state.proxy() as data:
		data['first_q'] = message.text
	await FSMClient.next()
	await message.reply('Как вы будете платить?')
#Повторяем полностью заказ
async def load_second(message : types.Message, state : FSMContext):
	async with state.proxy() as data:
		data['second_q'] = message.text
	await FSMClient.next()
	await bot.send_message(message.from_user.id,'Вы хотите ' + str(data['first_q']) + ' пиццу, оплата - ' + str(data['second_q'])+'?')
#Если все верно говорим спасибо иначе просим повторить заказ
async def last_quest(message : types.Message, state : FSMContext):
	if message.text == 'Да':
		async with state.proxy() as data:
			data['last_answer'] = message.text
			await message.reply('Спасибо за заказ!')
		await state.finish()
	else:
		await state.finish()
		await bot.send_message(message.from_user.id, 'Повторите заказ!')



#Дополнительные команды для общения с ботом
#отправляем пользователя в личку чтобы не флудить в группе
async def commands_start(message :types.Message):
	try:
		await bot.send_message(message.from_user.id,'Приятного аппетита', reply_markup=kb_client)
		await message.delete()
	except:
		await message.reply('Бот тут, напиши ему \nhttps://test_pizza_kvint_bot')
#обычные команды чтобы пользователю было не скучно
async def open_comand(message : types.Message):
	await bot.send_message(message.from_user.id,'Каждый день с 9:00 до 23:00')

async def place_comand(message : types.Message):
	await bot.send_message(message.from_user.id,'ул.Пушкина 01')
#чтение и бд
async def pizza_menu(message : types.Message):
	await pizza_db.sql_read(message)

def register_handlers_client(dp : Dispatcher):
	dp.register_message_handler(commands_start, commands=['start', 'help'])
	dp.register_message_handler(open_comand, commands=['Режим_работы'])
	dp.register_message_handler(place_comand, commands=['Расположение'])
	dp.register_message_handler(pizza_menu, commands=['Меню'])
	dp.register_message_handler(start_convers, commands=['Заказать'], state=None)
	dp.register_message_handler(load_first, state=FSMClient.first_q)
	dp.register_message_handler(load_second, state=FSMClient.second_q)
	dp.register_message_handler(last_quest, state=FSMClient.last_answer)
	dp.register_message_handler(make_changes_command, commands=['Меню'])